# OpenStreetMap Home Assignment

In this task we would like to extract some urban data regarding the area surrounding certain geographic location.

Consider a car moving in a constant direction, we would like to implement a function retrieving data in the area that the car is likely to be at in the near future. For example, if the car is moving east, we would like to extract all the data in the 200 meter east to the car, 100 meter west to the car, 75 meter north and 75 meter south to the car:

![Example area](https://bytebucket.org/fringefy/osm-assignment/raw/6d1cea724ae5ec1551c4a0b87bf9aeb0a05f9d22/example.png "Example area")

In order to extract the urban data, we will use [OpenStreetMap API](https://wiki.openstreetmap.org/wiki/API "OpenStreetMap API"). `OpenStreetMap API` provide various API calls to retrieve [OSM Nodes](https://wiki.openstreetmap.org/wiki/Node "OSM Nodes") (OpenStreetMap nodes), containing the required data regarding buildings in the area. Your task is to query the `OpenStreetMap API` in order to get the required data, and then implement a `python` or a `javascript` function, performing the work.
Your function should receive 3 parameters:

- `lat` - the GPS latitude value of the car.
- `lng` - the GPS longitude value of the car.
- `azimuth` - the angle in degrees between the magnetic north and the car direction

Your function should return an array of `OSM nodes`.
In this repository, you'll find both [python](https://bitbucket.org/fringefy/osm-assignment/raw/6d1cea724ae5ec1551c4a0b87bf9aeb0a05f9d22/assignment.py "python") and [javascript](https://bitbucket.org/fringefy/osm-assignment/raw/6d1cea724ae5ec1551c4a0b87bf9aeb0a05f9d22/assignment.js "javascript") templates to implement. Choose one of those languages, implement the task according to these instructions, and submit it.
Feel free to implement this task the best way you find suitable.
